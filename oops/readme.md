Concepts of Oops
===============
1) Abstraction :
---------------
                Concept of Hiding Implementation from user is called abstraction . We achieve abstraction in Java by using following ways :
                - > Interfaces
                - > Abstract Classes
2) Encapsulation :
----------------
                Concept of binding the data as well as functionality as a single unit is called encapsulation . Encapsulation also includes a concept of data hiding which means data inside the class can't be accessed by other classes directly . That is why always members of a class are declared as private and we always write getters and setters.
3) Inheritance :
----------------
                In java we can derive a class from another class . Derived class is called subclass and by doing this we can get all the properties and functionalities of the source/super class . This Concept is Called Inheritance . There are different types of inheritance . Only few types are supported by Java.

4) Polymorphism :
----------------
                Poly means many and morphs means forms . polymorhism in java can be seen in the context of methods. we see same method name but different behavious . So ,process of exhibiting multiple behaviours by having same method name is called concept of polymorphism. There are two types of polymorphims which can be seen in java . 
                1) Run-time polymorphism (Overriding)
                2)Compile-time polymorphism (OverLoading)

